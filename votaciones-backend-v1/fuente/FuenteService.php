<?php

include_once 'candidato/Candidato.php';
include_once 'response.php';

//Clase para llamar a las consultas sql
class CandidatoService
{
    public $candidato;
    public $response;

    function __construct()
    {
        $this->candidato =  new Candidato();
        $this->response =  new Response();
    }

    function index()
    {
        $res = $this->candidato->obtenerCandidatos();
        $candidatos = array();
        // Verifica si existe respuesta
        if ($res->rowCount()) {
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {

                $item = array(
                    "id" => $row['id'],
                    "nombre" => $row['nombre'],
                );
                array_push($candidatos, $item);
            }

            $this->response->success($candidatos);
        } else {
            $this->response->error_406("No se encontro ningun elemento");
        }
    }
}
