<?php

include_once 'fuente/FuenteService.php';

//Clase para ejecutar verificar los metdoso de la request, verificar los datos y llamar a los controladores
class FuenteController
{
    public $api;
    public $method;
    public $response;

    function __construct()
    {
        $this->api =  new FuenteService();
        $this->response =  new Response();
        $this->method = $_SERVER['REQUEST_METHOD'];
    }

    function index()
    {
        if ($this->method == 'GET') {
            $this->api->index();
        } else {
            $this->response->error_405();
        }
    }
}
