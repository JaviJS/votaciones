<?php
include_once 'region/RegionController.php';
include_once 'candidato/CandidatoController.php';
include_once 'fuente/FuenteController.php';
include_once 'votacion/VotacionController.php';
include_once 'comuna/ComunaController.php';
include_once 'response.php';
class Router
{
    public $region_controller;
    public $candidato_controller;
    public $fuente_controller;
    public $votacion_controller;
    public $comuna_controller;
    public $response;
    public $URL;
    function __construct()
    {
        $this->region_controller =  new RegionController();
        $this->candidato_controller = new CandidatoController();
        $this->fuente_controller = new FuenteController();
        $this->votacion_controller = new VotacionController();
        $this->comuna_controller = new ComunaController();
        $this->response = new Response();
        $this->URL = "/javiera_jara/v1/";
    }

    function routes($request)
    {
        $re = '/\/javiera_jara\/v1\/comunas\/[0-9]+/';
        switch ($request) {
            case $this->URL . 'regiones':
                $this->navigate($this->region_controller, "index");
                break;
            case (preg_match($re, $request) == 1):
                //Acepta solo /comunas/[numero]
                $_GET["id"] = str_replace($this->URL . 'comunas/', "", $request);
                $this->navigate($this->comuna_controller, "index");
                break;
            case $this->URL . 'candidatos':
                $this->navigate($this->candidato_controller, "index");
                break;
            case $this->URL . 'fuentes':
                $this->navigate($this->fuente_controller, "index");
                break;
            case $this->URL . 'votaciones':
                $this->navigate($this->votacion_controller, "index");
                break;
            case $this->URL . 'votaciones/agregarVotacion':
                $this->navigate($this->votacion_controller, "store", true);
                break;
            default:
                $this->response->error_404();
                break;
        }
    }
    function navigate($class, $route)
    {
        call_user_func(array($class, $route));
    }
}
