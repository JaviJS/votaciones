<?php

include_once 'db.php';

//Clase para ejecutar las consultas sql
class Usuario extends DB
{

    function nuevoUsuario($usuario)
    {
        $connection = $this->connect();
        $query = $connection->prepare('INSERT INTO usuario (nombre, alias, rut, email) VALUES (:nombre, :alias, :rut, :email);');
        $query->execute([
            'nombre' => $usuario['nombre'],
            'alias' => $usuario['alias'],
            'rut' => $usuario['rut'],
            'email' => $usuario['email']
        ]);
        $ultimoId = $connection->lastInsertId();
        return intval($ultimoId);
    }

    function buscarUsuario($rut)
    {
        $query = $this->connect()->query('SELECT COUNT(*) FROM usuario where rut=' . $rut);
        return $query->fetchColumn();
    }
}
