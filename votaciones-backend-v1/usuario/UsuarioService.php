<?php

include_once 'usuario/Usuario.php';
include_once 'response.php';

//Clase para llamar a las consultas sql
class UsuarioService
{
    public $usuario;
    public $response;

    function __construct()
    {
        $this->usuario =  new Usuario();
        $this->response =  new Response();
    }

    function add($item)
    {
        $res = $this->usuario->nuevoUsuario($item);
        return $res;
    }

    function find($rut)
    {
        $res = $this->usuario->buscarUsuario($rut);
        return $res;
    }
}
