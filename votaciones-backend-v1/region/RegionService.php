<?php

include_once 'region/Region.php';
include_once 'response.php';

//Clase para llamar a las consultas sql
class RegionService
{
    public $region;
    public $response;

    function __construct()
    {
        $this->region =  new Region();
        $this->response =  new Response();
    }

    function index()
    {
        $res = $this->region->obtenerRegiones();
        $regiones = array();
        // Verifica si existe respuesta
        if ($res->rowCount()) {
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {

                $item = array(
                    "id" => $row['id'],
                    "nombre" => $row['nombre'],
                );
                array_push($regiones, $item);
            }

            $this->response->success($regiones);
        } else {
            $this->response->error_406("No se encontro ningun elemento");
        }
    }
}
