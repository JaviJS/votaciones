<?php

include_once 'db.php';

//Clase para ejecutar las consultas sql
class Votacion extends DB
{

    function obtenerVotaciones()
    {
        $query = $this->connect()->query('SELECT * FROM votacion');
        return $query;
    }

    function nuevaVotacion($votacion)
    {
        $connection = $this->connect();
        $query = $connection->prepare('INSERT INTO votacion (candidato_id, region_id, comuna_id, usuario_id) VALUES (:candidato_id, :region_id, :comuna_id, :usuario_id)');
        $query->execute([
            'candidato_id' => $votacion['candidato_id'],
            'region_id' => $votacion['region_id'],
            'comuna_id' => $votacion['comuna_id'],
            'usuario_id' => $votacion['usuario_id']
        ]);
        $ultimoId = $connection->lastInsertId();
        return intval($ultimoId);
    }

    function nuevaVotacionFuente($votacion_fuente)
    {
        $connection = $this->connect();
        $query = $connection->prepare('INSERT INTO votacion_fuente (votacion_id, fuente_id) VALUES (:votacion_id, :fuente_id)');
        $query->execute([
            'votacion_id' => $votacion_fuente['votacion_id'],
            'fuente_id' => $votacion_fuente['fuente_id']
        ]);
        $ultimoId = $connection->lastInsertId();
        return intval($ultimoId);
    }
}
