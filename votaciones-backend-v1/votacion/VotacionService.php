<?php

include_once 'votacion/Votacion.php';
include_once 'response.php';

//Clase para llamar a las consultas sql
class VotacionService
{
    public $votacion;
    public $response;

    function __construct()
    {
        $this->votacion =  new Votacion();
        $this->response =  new Response();
    }

    function index()
    {
        $res = $this->votacion->obtenerVotaciones();
        $votaciones = array();
        // Verifica si existe respuesta
        if ($res->rowCount()) {
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {

                $item = array(
                    "id" => $row['id'],
                    "usuario_id" => $row['usuario_id'],
                    "candidato_id" => $row['candidato_id'],
                    "region_id" => $row['region_id'],
                    "comuna_id" => $row['comuna_id'],
                );
                array_push($votaciones, $item);
            }

            $this->response->success($votaciones);
        } else {
            $this->response->error_406("No se encontro ningun elemento");
        }
    }

    function add($item)
    {
        $res = $this->votacion->nuevaVotacion($item);
        return $res;
    }

    function addVotacionFuente($item)
    {
        $res = $this->votacion->nuevaVotacionFuente($item);
        return $res;
    }
}
