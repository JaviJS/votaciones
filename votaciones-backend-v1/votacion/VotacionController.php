<?php

include_once 'votacion/VotacionService.php';
include_once 'usuario/UsuarioService.php';

//Clase para ejecutar verificar los metdoso de la request, verificar los datos y llamar a los controladores
class VotacionController
{
    public $apiVotacion;
    public $apiUsuario;
    public $method;
    public $response;

    function __construct()
    {
        $this->apiVotacion =  new VotacionService();
        $this->apiUsuario = new UsuarioService();
        $this->response =  new Response();
        $this->method = $_SERVER['REQUEST_METHOD'];
    }

    function index()
    {
        if ($this->method == 'GET') {
            $this->apiVotacion->index();
        } else {
            $this->response->error_405();
        }
    }

    function store()
    {
        $campos_usuario = ['nombre', 'alias', 'rut', 'email'];
        $campos_votacion = ['candidato_id', 'region_id', 'comuna_id'];
        $campos_fuentes = ['fuentes'];
        $errors = [];
        $values_usuario = [];
        $values_votacion = [];
        $values_fuentes = [];

        if (!empty($_POST)) {
            $data = $_POST;
        } else {
            $data = json_decode(file_get_contents('php://input'), true);
        }


        if ($this->method == 'POST') {
            //Recorres los campos de POST y verifica que no esten vacios
            foreach ($campos_usuario as $campo) {
                if (empty($data[$campo])) {
                    $errors[] = $campo;
                } else {
                    $values_usuario[$campo] = $data[$campo];
                }
            }
            foreach ($campos_votacion as $campo) {
                if (empty($data[$campo])) {
                    $errors[] = $campo;
                } else {
                    $values_votacion[$campo] = $data[$campo];
                }
            }
            foreach ($campos_fuentes as $campo) {
                if (empty($data[$campo])) {
                    $errors[] = $campo;
                } else {
                    $values_fuentes[$campo] = $data[$campo];
                }
            }

            $existeUsuario = $this->apiUsuario->find($data['rut']);
            if ($existeUsuario == 1) {
                return $this->response->error_existe_elemento();
            }

            //Verifica si el array de errors esta vacio
            if (empty($errors)) {
                $usuario_id = $this->apiUsuario->add($values_usuario);
                $values_votacion['usuario_id'] = $usuario_id;
                $votacion_id = $this->apiVotacion->add($values_votacion);
                foreach ($values_fuentes as $fuentes) {
                    foreach($fuentes as $fuente){
                        $fuente_data['votacion_id'] = $votacion_id;
                        $fuente_data['fuente_id'] = $fuente;
                        $this->apiVotacion->addVotacionFuente($fuente_data);
                    }
                }

                $this->response->success("Votación realizada con éxito.");
            } else {
                $this->response->error_400();
            }
        } else {
            $this->response->error_405();
        }
    }
}
