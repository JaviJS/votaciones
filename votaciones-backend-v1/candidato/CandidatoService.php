<?php

include_once 'fuente/Fuente.php';
include_once 'response.php';

//Clase para llamar a las consultas sql
class FuenteService
{
    public $fuente;
    public $response;

    function __construct()
    {
        $this->fuente =  new Fuente();
        $this->response =  new Response();
    }

    function index()
    {
        $res = $this->fuente->obtenerFuentes();
        $fuentes = array();
        // Verifica si existe respuesta
        if ($res->rowCount()) {
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {

                $item = array(
                    "id" => $row['id'],
                    "nombre" => $row['nombre'],
                );
                array_push($fuentes, $item);
            }

            $this->response->success($fuentes);
        } else {
            $this->response->error_406("No se encontro ningun elemento");
        }
    }
}
