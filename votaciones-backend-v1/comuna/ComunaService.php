<?php

include_once 'comuna/Comuna.php';
include_once 'response.php';

//Clase para llamar a las consultas sql
class ComunaService
{
    public $comuna;
    public $response;

    function __construct()
    {
        $this->comuna =  new Comuna();
        $this->response =  new Response();
    }

    function index($id)
    {
        $res = $this->comuna->obtenerComunas($id);
        $comunas = array();
        // Verifica si existe respuesta
        if ($res->rowCount()) {
            while ($row = $res->fetch(PDO::FETCH_ASSOC)) {

                $item = array(
                    "id" => $row['id'],
                    "nombre" => $row['nombre'],
                );
                array_push($comunas, $item);
            }

            $this->response->success($comunas);
        } else {
            $this->response->error_406("No se encontro ningun elemento");
        }
    }
}
