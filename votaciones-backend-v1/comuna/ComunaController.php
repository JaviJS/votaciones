<?php

include_once 'comuna/ComunaService.php';

//Clase para ejecutar verificar los metdoso de la request, verificar los datos y llamar a los controladores
class ComunaController
{
    public $api;
    public $method;
    public $response;

    function __construct()
    {
        $this->api =  new ComunaService();
        $this->response =  new Response();
        $this->method = $_SERVER['REQUEST_METHOD'];
    }

    function index()
    {
        if ($this->method == 'GET') {
            if (isset($_GET['id'])) {
                $this->api->index($_GET['id']);
            } else {
                $this->response->error_400();
            }
        } else {
            $this->response->error_405();
        }
    }
}
