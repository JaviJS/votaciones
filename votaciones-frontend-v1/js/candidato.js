const selectCandidatos = document.getElementById("candidatos");

document.addEventListener("DOMContentLoaded", async function () {
   const candidatos = await getCandidatos();
   const option = document.createElement("option");
   option.value= "";
   option.textContent = "Seleccione una opción";
   option.disabled = true;
   option.selected = true;
   selectCandidatos.appendChild(option);
   candidatos.result.forEach(candidato => {
    const candidato_opcion = document.createElement("option");
    candidato_opcion.textContent = candidato.nombre;
    candidato_opcion.value = candidato.id; 
    selectCandidatos.appendChild(candidato_opcion);
   });
  });


async function getCandidatos() {
    try {
        const response = await fetch(
          "http://localhost:8000/javiera_jara/v1/candidatos"
        );
    
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
    
        const responseData = await response.json();
        return responseData; // Devolver los datos de la respuesta
      } catch (error) {
        console.error("There was a problem with the fetch operation:", error);
        throw error; // Lanzar el error para ser manejado por el código que llame a la función
      }
}