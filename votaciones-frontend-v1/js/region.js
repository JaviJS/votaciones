const selectRegiones = document.getElementById("regiones");

document.addEventListener("DOMContentLoaded", async function () {
   const regiones = await getRegiones();
   const option = document.createElement("option");
   option.value= "";
   option.textContent = "Seleccione una opción";
   option.disabled = true;
   option.selected = true;
   selectRegiones.appendChild(option);
   regiones.result.forEach(region => {
    const regiones_opcion = document.createElement("option");
    regiones_opcion.textContent = region.nombre;
    regiones_opcion.value = region.id; 
    selectRegiones.appendChild(regiones_opcion);
   });
  });


async function getRegiones() {
    try {
        const response = await fetch(
          "http://localhost:8000/javiera_jara/v1/regiones"
        );
    
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
    
        const responseData = await response.json();
        return responseData; // Devolver los datos de la respuesta
      } catch (error) {
        console.error("There was a problem with the fetch operation:", error);
        throw error; // Lanzar el error para ser manejado por el código que llame a la función
      }
}