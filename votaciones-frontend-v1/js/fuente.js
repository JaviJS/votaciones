const selectFuentes = document.getElementById("candidatos");

document.addEventListener("DOMContentLoaded", async function () {
   // Obtén la referencia al contenedor donde se cargarán las opciones
  var contenedorOpciones = document.getElementById("contenedorFuentes");

  // Obtener fuentes
  const fuentes = await getFuentes();

  // Itera sobre la lista de opciones y crea checkboxes
  fuentes.result.forEach(function(opcion) {
    var checkboxLabel = document.createElement("label");
    checkboxLabel.innerHTML = `<input id="${opcion.nombre}" type="checkbox" name="fuentes" value="${opcion.id}"> ${opcion.nombre}`;
    
    // Agrega el checkbox al contenedor
    contenedorOpciones.appendChild(checkboxLabel);
  });
  });


async function getFuentes() {
    try {
        const response = await fetch(
          "http://localhost:8000/javiera_jara/v1/fuentes"
        );
    
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
    
        const responseData = await response.json();
        return responseData; // Devolver los datos de la respuesta
      } catch (error) {
        console.error("There was a problem with the fetch operation:", error);
        throw error; // Lanzar el error para ser manejado por el código que llame a la función
      }
}