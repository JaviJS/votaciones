const selectComunas = document.getElementById("comunas");

async function obtenerComunas() {
    const comunas = await obtenerComunasAPI(selectRegiones.value);
    selectComunas.innerHTML = "";
    const option = document.createElement("option");
    option.value = "";
    option.textContent = "Seleccione una opción";
    option.disabled = true;
    option.selected = true;
    selectComunas.appendChild(option);
    comunas.result.forEach(comuna => {
        const comunas_opcion = document.createElement("option");
        comunas_opcion.textContent = comuna.nombre;
        comunas_opcion.value = comuna.id;
        selectComunas.appendChild(comunas_opcion);
    });
}

async function obtenerComunasAPI(id) {
    try {
        const response = await fetch(
            "http://localhost:8000/javiera_jara/v1/comunas/" + id
        );

        if (!response.ok) {
            throw new Error("Network response was not ok");
        }

        const responseData = await response.json();
        return responseData; // Devolver los datos de la respuesta
    } catch (error) {
        console.error("There was a problem with the fetch operation:", error);
        throw error; // Lanzar el error para ser manejado por el código que llame a la función
    }
}