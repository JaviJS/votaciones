document.getElementById("formularioVotacion").addEventListener("submit", handleSubmit);

function handleSubmit(event) {
    event.preventDefault();

    // Obtener los valores de los campos del formulario
    const nombre = document.getElementById("nombre").value;
    const alias = document.getElementById("alias").value;
    const rut = document.getElementById("rut").value;
    const email = document.getElementById("email").value;
    const candidato = document.getElementById("candidatos").value;
    const region = document.getElementById("regiones").value;
    const comuna = document.getElementById("comunas").value;
    const contenedor = document.getElementById("contenedorFuentes");
    const fuentesCheckbox = contenedor.querySelectorAll('input[type="checkbox"]:checked');
    const fuentes = Array.from(fuentesCheckbox).map(checkbox => parseInt(checkbox.value));
    console.log({ nombre, alias, rut, email, candidato, region, comuna, fuentes });
    console.log(fuentes);
    const data = {
        nombre: nombre,
        alias: alias,
        rut: rut,
        email: email,
        candidato_id: parseInt(candidato),
        region_id: parseInt(region),
        comuna_id: parseInt(comuna),
        fuentes: fuentes
    };

    // Validar los campos del formulario
    const isValid = validateForm(data);
    if (isValid) {
        enviarVotacion(data);
    } else {
        displayMessage("Formulario incorrecto");
    }
}

// Función para validar los campos del formulario
function validateForm(data) {
    if (!data.nombre) return false;
    if (!validarAlias(data.alias)) return false;
    if (!validarRut(data.rut)) return false;
    if (!validarEmail(data.email)) return false;
    if (!data.candidato_id) return false;
    if (!data.region_id) return false;
    if (!data.comuna_id) return false;
    if (data.fuentes.length === 0) return false;
    return true;
}

const validarAlias = (alias) => {
    const expresionRegular = /^(?=.*[a-zA-Z])(?=.*[0-9]).{5,}$/;
    return expresionRegular.test(alias);
};
const validarRut = (rut) => {
    const expresionRegular = /^[0-9]{7,8}-[0-9K]$/;
    return expresionRegular.test(rut) && validateRut(rut);
};
const validateRut = (rutCompleto) => {
    console.log("rutcompleto");
    var tmp = rutCompleto.split("-");
    var digv = tmp[1];
    var rut = tmp[0];
    if (digv == "K") digv = "k";
    return dv(rut) == digv;
};
const dv = (T) => {
    var M = 0,
        S = 1;
    for (; T; T = Math.floor(T / 10))
        S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
    return S ? S - 1 : "k";
};
const validarEmail = (email) => {
    const expresionRegular = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return expresionRegular.test(email);
};
async function enviarVotacion(data) {

    try {
        const responseData = await guardarVotacion(data);

        console.log("Datos guardados con éxito:", responseData);
        displayMessage("Datos guardados con éxito");
    } catch (error) {
        console.error("Error en la solicitud:", error);
        displayMessage("Ha ocurrido un error");
    }
}

async function guardarVotacion(data) {
    try {
        const opciones = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        };

        const response = await fetch(
            "http://localhost:8000/javiera_jara/v1/votaciones/agregarVotacion",
            opciones
        );

        if (!response.ok) {
            throw new Error("Network response was not ok");
        }

        const responseData = await response.json();
        return responseData; // Devolver los datos de la respuesta
    } catch (error) {
        displayMessage("Ha ocurrido un error");
        throw error; // Lanzar el error para ser manejado por el código que llame a la función
    }
}


function displayMessage(message) {
    const divMensaje = document.getElementById("mensaje");
    divMensaje.textContent = message;

    setTimeout(() => {
        divMensaje.textContent = "";
    }, 2000);
}